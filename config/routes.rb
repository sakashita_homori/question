Rails.application.routes.draw do
 get 'tops/main'
 post 'tops/login'
 root 'tops#main'
 get 'tops/logout'
 get 'main/anketo'
 root 'main#anketo'
 resources :users, only:[:index, :new, :create, :destroy]
end
